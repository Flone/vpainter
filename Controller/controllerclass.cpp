#include "controllerclass.h"
#include "Model/FacadeStuff/editorfacade.h"
#include "viewwindow.h"

#include "Model/States/activestate.h"
#include "Model/States/emptystate.h"
#include "Model/States/multiselection.h"
#include "Model/States/objcreation.h"
#include "Model/States/objdrag.h"
#include "Model/States/singleselect.h"

ControllerClass::ControllerClass(ViewWindow *View)
{
    pEditor = new EditorFacade(View,this);

    // 'Cursor' instrument is current instrument
    ButtonID = 10;

    // Create all states
    pEmptyState = new EmptyState(pEditor,this);

    pObjDrag = new ObjDrag(pEditor,this);

    pSingleSelect = new SingleSelect(pEditor,this);

    pObjCreation = new ObjCreation(pEditor,this);

    pMultiSelection = new MultiSelection(pEditor,this);

    pActiveState = new ActiveState(pEditor, this,pEmptyState);

    LastTimeMouseButton = false;

    CtrlPressed = false;
}

void ControllerClass::SceneMouseDownOrUp(int x, int y)
{
    if (LastTimeMouseButton == false)
    {
        pActiveState->MouseDown(x, y, ButtonID, CtrlPressed);
        LastTimeMouseButton = true;
    }
    else
    {
        pActiveState->MouseUp(x,y,ButtonID);
        LastTimeMouseButton = false;
    }
}

void ControllerClass::MouseMove(int x, int y)
{
    pActiveState->MouseMove(x,y);
}

void ControllerClass::MouseButtonPressed()
{
    ButtonID = 10;
}

void ControllerClass::CtrlChange(bool Pressed)
{
    CtrlPressed = Pressed;
}

void ControllerClass::LineButtonPressed()
{
    ButtonID = 0;
}

void ControllerClass::RectButtonPressed()
{
    ButtonID = 1;
}

void ControllerClass::EllipseButtonPressed()
{
    ButtonID = 2;
}

void ControllerClass::GroupButtonPressed()
{
    /*if (ButtonID == 3)
    {
        ButtonID = 10;
    }
    else */ButtonID = 3;
}

void ControllerClass::ZPressed()
{
    if (CtrlPressed)
    {
        pEditor->Undo();
    }
}

void ControllerClass::SetNewActiveState(unsigned char NewState)
{
    switch(NewState)
    {
    case(0):
        pEditor->SetTextGroupOff(false);
        pEditor->ShowUnShowGroupButton(false);
        pActiveState->SetCurrentState(pEmptyState);
        break;
    case(1):
        pEditor->SetTextGroupOff(false);
        pEditor->ShowUnShowGroupButton(false);
        pActiveState->SetCurrentState(pObjDrag);
        break;
    case(2):
        pEditor->SetTextGroupOff(true);
        pEditor->ShowUnShowGroupButton(true);
        pActiveState->SetCurrentState(pSingleSelect);
        break;
    case(3):
        pEditor->ShowUnShowGroupButton(false);
        pActiveState->SetCurrentState(pObjCreation);
        break;
    case(4):
        pEditor->SetTextGroupOff(false);
        pEditor->ShowUnShowGroupButton(true);
        pActiveState->SetCurrentState(pMultiSelection);
        break;
    }
}

ControllerClass::~ControllerClass()
{
    // Delete all states
    delete pActiveState;

    delete pEmptyState;

    delete pObjDrag;

    delete pSingleSelect;

    delete pObjCreation;

    delete pMultiSelection;

    // Delete Editor
    delete pEditor;
}
