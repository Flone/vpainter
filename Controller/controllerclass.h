#pragma once

class ViewWindow;
class EditorFacade;

// All states
class ActiveState;
class EmptyState;
class ObjDrag;
class SingleSelect;
class ObjCreation;
class MultiSelection;
class QShortcut;

class ControllerClass
{
public:

    ControllerClass(ViewWindow *ui);

    void SceneMouseDownOrUp(int x, int y);

    void MouseMove(int x, int y);

    void MouseButtonPressed();

    void CtrlChange(bool Pressed);

    void LineButtonPressed();

    void RectButtonPressed();

    void EllipseButtonPressed();

    void GroupButtonPressed();

    void ZPressed();

    void SetNewActiveState(unsigned char NewState);

    ~ControllerClass();

    // ID of the last button that the user pressed
    unsigned char ButtonID;

private:

    EditorFacade* pEditor;

    // All states
    ActiveState*    pActiveState;

    EmptyState*     pEmptyState;

    ObjDrag*        pObjDrag;

    SingleSelect*   pSingleSelect;

    ObjCreation*    pObjCreation;

    MultiSelection* pMultiSelection;

    bool LastTimeMouseButton;

    bool CtrlPressed;
};
