#pragma once


class Command
{
public:

    Command();

    virtual void Undo() = 0;

    virtual ~Command() {}
};
