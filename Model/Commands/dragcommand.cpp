#include "dragcommand.h"

#include "Model/GraphItemAndChilds/graphitem.h"
#include "Model/FacadeStuff/editorfacade.h"
#include "Model/frameclass.h"

DragCommand::DragCommand(GraphItem* pItem,  EditorFacade* pFacade)
{
    this->pFacade = pFacade;

    this->pItem = pItem;

    x1 = pItem->FrameObj.x1;
    x2 = pItem->FrameObj.x2;
    y1 = pItem->FrameObj.y1;
    y2 = pItem->FrameObj.y2;
}

void DragCommand::Undo()
{
    FrameClass Frame;
    Frame.x1 = x1;
    Frame.x2 = x2;
    Frame.y1 = y1;
    Frame.y2 = y2;

    pItem->SetFrame(Frame);
    pFacade->RecalculateSelections();
}
