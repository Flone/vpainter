#pragma once
#include "Model/Commands/command.h"

class GraphItem;
class EditorFacade;

class DragCommand : public Command
{
public:

    DragCommand(GraphItem* pItem, EditorFacade* pFacade);

    virtual void Undo() override;

    virtual ~DragCommand() override {}

private:

    EditorFacade* pFacade;

    GraphItem* pItem;

    int x1;
    int x2;
    int y1;
    int y2;
};
