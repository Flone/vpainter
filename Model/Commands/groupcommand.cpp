#include "groupcommand.h"
#include "Model/FacadeStuff/editorfacade.h"

GroupCommand::GroupCommand(GraphItem *pGroup, EditorFacade *pEditorFacade)
{
    this->pEditor = pEditorFacade;
    this->pGroup = pGroup;
}

void GroupCommand::Undo()
{
    pEditor->UnGroupByItem(pGroup);
}
