#pragma once
#include "Model/Commands/command.h"

class GraphItem;
class EditorFacade;

class GroupCommand : public Command
{
public:

    GroupCommand(GraphItem* pGroup, EditorFacade* pEditorFacade);

    virtual void Undo() override;

    virtual ~GroupCommand() override {}

private:

    GraphItem* pGroup;

    EditorFacade* pEditor;
};
