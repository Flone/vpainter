#include "objcreatecommand.h"
#include "Model/FacadeStuff/editorfacade.h"

ObjCreateCommand::ObjCreateCommand(GraphItem* pElement, EditorFacade* pFacade)
{
    this->pFacade = pFacade;
    this->pElement = pElement;
}

void ObjCreateCommand::Undo()
{
    pFacade->DeleteElement(pElement);
}
