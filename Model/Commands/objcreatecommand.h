#pragma once
#include "Model/Commands/command.h"

class EditorFacade;
class GraphItem;

class ObjCreateCommand : public Command
{
public:

    ObjCreateCommand(GraphItem* pElement, EditorFacade* pFacade);

    virtual void Undo() override;

    virtual ~ObjCreateCommand() override {}

private:

    GraphItem* pElement;

    EditorFacade* pFacade;
};
