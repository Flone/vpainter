#include "ungroupcommand.h"
#include "Model/FacadeStuff/editorfacade.h"

UnGroupCommand::UnGroupCommand(GraphItem *pGroup, EditorFacade *pFacade)
{
    this->pFacade = pFacade;
    this->pGroup  = pGroup;
}

void UnGroupCommand::Undo()
{
    pFacade->GroupUndo(pGroup);
}
