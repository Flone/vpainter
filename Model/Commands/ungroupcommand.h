#pragma once
#include "Model/Commands/command.h"

class GraphItem;
class EditorFacade;

class UnGroupCommand : public Command
{
public:

    UnGroupCommand(GraphItem* pGroup, EditorFacade* pFacade);

    virtual void Undo() override;

    virtual ~UnGroupCommand() override {}

private:

    GraphItem*    pGroup;

    EditorFacade* pFacade;
};
