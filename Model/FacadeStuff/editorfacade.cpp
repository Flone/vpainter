#include "viewwindow.h"
#include "ui_viewwindow.h"

#include "editorfacade.h"
#include "Controller/controllerclass.h"
#include "Model/FacadeStuff/sceneclass.h"
#include "Model/FacadeStuff/factory.h"
#include "Model/FacadeStuff/storeclass.h"
#include "Model/FacadeStuff/painterclass.h"
#include "Model/GraphItemAndChilds/group.h"

#include "Model/Commands/objcreatecommand.h"
#include "Model/Commands/groupcommand.h"
#include "Model/Commands/ungroupcommand.h"
#include "Model/Commands/dragcommand.h"

#include <typeinfo.h>


EditorFacade::EditorFacade(ViewWindow *View, ControllerClass *pControl)
{
    this->View = View;

    // Create a store
    pStore = new StoreClass();

    // Create a scene
    pScene = new SceneClass(pControl,pStore,View);

    // Set the scene to View
    View->ui->verticalLayout_4->addWidget(pScene);

    // Create a factory
    pFactory = new Factory(pStore);

    // Create painter
    PeterThePainter = new PainterClass(pScene);

    pController = pControl;
}

bool EditorFacade::CheckHit(int x, int y)
{
    if (pScene->CheckHit(x,y))
    {
        // Hit something
        // Update Scene because there can appear some selection points
        pScene->Draw(PeterThePainter);

        return true;
    }
    return false;
}

void EditorFacade::CreateObject(int x, int y, unsigned char ButtonID)
{
    if (pFactory->MouseDownOrUp(x,y,ButtonID))
    {
        GraphItem* pItem = pStore->GetLastElement();
        if (typeid(*pItem) == typeid(Group))
        {
            // We made a group
            GroupCommand* pGroupCommand = new GroupCommand(pItem,this);
            pStore->AddCommand(pGroupCommand);
        }
        else
        {
            // Object was created
            ObjCreateCommand* pObjCreateCommand = new ObjCreateCommand(pStore->Items[pStore->Items.size() - 1],this);
            pStore->AddCommand(pObjCreateCommand);
        }

        pScene->Draw(PeterThePainter);
        return;
    }
}

void EditorFacade::MoveTo(int x, int y)
{
    if (isGrabbed)
    {
        // If user will do Undo() then command will: Change frame and then recalculate selection if it's exists
        DragCommand* pCommand = new DragCommand(pStore->GetGrabbedItem(),this);
        pStore->AddCommand(pCommand);
    }

    pStore->MoveTo(x,y);

    // Update Scene
    pScene->Draw(PeterThePainter);
}

bool EditorFacade::TryGrab(int x, int y)
{
    if (pStore->TryGrab(x,y)) return true;
    else return false;
}

void EditorFacade::MouseDownOnSelectionPoint(bool isGrabbed)
{
    this->isGrabbed = isGrabbed;
}

void EditorFacade::Undo()
{
    pStore->Undo();

    // Update Scene
    pScene->Draw(PeterThePainter);
}

void EditorFacade::DeleteElement(GraphItem *pElement)
{
    pStore->DeleteElement(pElement);

    pController->ButtonID = 10;
    pController->SetNewActiveState(0);

    pScene->Draw(PeterThePainter);
}

void EditorFacade::GroupUndo(GraphItem* pGroup)
{
    pStore->GroupUndo(pGroup);

    pController->ButtonID = 10;
    pController->SetNewActiveState(0);

    pScene->Draw(PeterThePainter);
}

void EditorFacade::RecalculateSelections()
{
    pStore->RecalculateSelections();

    pScene->Draw(PeterThePainter);
}

void EditorFacade::SetTextGroupOff(bool on)
{
    if (on) View->ui->groupButton->setText("GroupOff");
    else View->ui->groupButton->setText("GroupOn");
}

void EditorFacade::ShowUnShowGroupButton(bool show)
{
    if (show) View->ui->groupButton->setEnabled(true);
    else View->ui->groupButton->setEnabled(false);
}

void EditorFacade::UnGroup()
{
    pStore->UnGroup();
    GraphItem* pGroup = pStore->GetLastGroup();
    if (pGroup != nullptr)
    {
       UnGroupCommand* pCommand = new UnGroupCommand(pGroup,this);
       pStore->AddCommand(pCommand);
    }
}

void EditorFacade::UnGroupByItem(GraphItem *pGroup)
{
    pStore->UnGroupByItem(pGroup);

    pController->ButtonID = 10;
    pController->SetNewActiveState(0);

    pScene->Draw(PeterThePainter);
}

void EditorFacade::DeleteAllSelections()
{
    pStore->DeleteAllSelections();

    // Update Scene
    pScene->Draw(PeterThePainter);
}

EditorFacade::~EditorFacade()
{
    delete PeterThePainter;
    delete pStore;
    delete pFactory;
    delete pScene;
}
