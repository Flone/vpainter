#pragma once

class ViewWindow;
class ControllerClass;
class SceneClass;
class Factory;
class StoreClass;
class PainterClass;
class Command;
class GraphItem;

class EditorFacade
{
public:

    EditorFacade(ViewWindow *ui, ControllerClass *pControl);

    bool CheckHit(int x, int y);

    void CreateObject(int x, int y, unsigned char ButtonID);

    void MoveTo(int x, int y);

    bool TryGrab(int x, int y);

    void MouseDownOnSelectionPoint(bool isGrabbed);

    void Undo();

    void DeleteElement(GraphItem* pElement);

    void GroupUndo(GraphItem* pGroup);

    void RecalculateSelections();

    void SetTextGroupOff(bool on);

    void ShowUnShowGroupButton(bool show);

    void UnGroup();

    void UnGroupByItem(GraphItem* pGroup);

    void DeleteAllSelections();

    ~EditorFacade();

private:

    // Pointer to View
    ViewWindow   *View;

    // Scene Widget on which we will paint
    SceneClass   *pScene;

    // Factory will tell painter when paint items on scene
    Factory      *pFactory;

    // Store will contain all items on scene
    StoreClass   *pStore;

    // Painter will paint items on scene
    PainterClass *PeterThePainter;

    // We need that to change states when Undo() was made
    ControllerClass* pController;

    bool isGrabbed;
};
