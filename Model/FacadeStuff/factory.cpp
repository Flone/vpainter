#include "factory.h"
#include "Model/FacadeStuff/storeclass.h"
#include "Model/PropsClasses/propbox.h"

#include "Model/GraphItemAndChilds/group.h"
#include "Model/GraphItemAndChilds/lineclass.h"
#include "Model/GraphItemAndChilds/rectclass.h"
#include "Model/GraphItemAndChilds/ellipseclass.h"

#include "Model/SelectionClasses/selection.h"

#include <vector>


Factory::Factory(StoreClass *pStore)
{
    this->pStore = pStore;
    CurrentState = 0;
}

bool Factory::MouseDownOrUp(int x, int y, unsigned char Type)
{
    if (Type == 3)
    {
        Group* NewGroup = new Group();

        std::vector<Selection*> Selections = pStore->ReturnAllSelections();

        // Add elements to Group
        for (unsigned int i = 0; i < Selections.size(); i++)
        {
            NewGroup->AddToGroup(Selections[i]->Item);
        }

        pStore->AddItem(NewGroup, Type);

        return true;
    }
    else
    {
        if (CurrentState == 0)
        {
            // Get cursor position
            TempFrame.x1 = x;
            TempFrame.y1 = y;

            // Set state to 'waiting for second point'
            CurrentState = 1;

            return false;
        }
        else
        {
            // Get cursor position
            TempFrame.x2 = x;
            TempFrame.y2 = y;

            // Set state to default state
            CurrentState = 0;

            // Set default props to new item
            PropBox* NewProps = new PropBox();
            PenProp NewPen; NewPen.PenRColor = 0; NewPen.PenGColor = 0; NewPen.PenBColor = 0;
            BrushProp NewBrush; NewBrush.BrushRColor = 250; NewBrush.BrushGColor = 10; NewBrush.BrushBColor = 10;
            NewProps->setParams(NewPen, NewBrush);

            if (Type == 0)
            {
                // Add new line to Store
                LineClass* Line = new LineClass(TempFrame, NewProps);
                pStore->AddItem(Line, Type);
            }
            else if (Type == 1)
            {
                // Add new rectangle to Store
                RectClass* NewRect = new RectClass(TempFrame, NewProps);
                pStore->AddItem(NewRect, Type);
            }
            else
            {
                // Add new ellipse to Store
                EllipseClass* NewEllipse = new EllipseClass(TempFrame, NewProps);
                pStore->AddItem(NewEllipse, Type);
            }

            return true;
        }
    }
}
