#pragma once
#include "Model/frameclass.h"

class StoreClass;

class Factory
{
public:
    Factory(StoreClass *pStore);
    bool MouseDownOrUp(int x, int y, unsigned char Type);
private:
    StoreClass *pStore;

    // Contains current state of item creation
    unsigned char CurrentState;

    // This will contain frame of new item
    FrameClass TempFrame;
};
