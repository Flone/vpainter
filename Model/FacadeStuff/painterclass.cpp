#include "painterclass.h"
#include "Model/frameclass.h"
#include "Model/FacadeStuff/sceneclass.h"
#include "Model/PropsClasses/propbox.h"

#include <QPainter>

PainterClass::PainterClass(SceneClass *pScene)
{
    this->pScene = pScene;
}

void PainterClass::paintEllipse(FrameClass Frame, PropBox *ItemProps)
{
    // Start to paint in the buffer
    QPainter p(pScene->GetBuffer());
    p.setPen(QPen(Qt::red,1,Qt::SolidLine));
    p.drawEllipse(Frame.x1,Frame.y1,Frame.x2-Frame.x1,Frame.y2-Frame.y1);
}

void PainterClass::paintBorder(FrameClass Frame)
{
    // Start to paint in the buffer
    QPainter p(pScene->GetBuffer());
    p.setPen(QPen(Qt::darkBlue,1,Qt::SolidLine));
    p.drawRect(Frame.x1,Frame.y1,Frame.x2-Frame.x1,Frame.y2-Frame.y1);
}

void PainterClass::DrawSelectionPoint(int x, int y, int delta)
{
    QPainter p(pScene->GetBuffer());
    p.setPen(QPen(Qt::black,1,Qt::SolidLine));

    int BeginX = x - (delta/2);
    int BeginY = y - (delta/2);

    p.drawRect(BeginX,BeginY,delta,delta);
}

void PainterClass::paintLine(FrameClass Frame, PropBox *ItemProps)
{
    // Start to paint in the buffer
    QPainter p(pScene->GetBuffer());
    p.setPen(QPen(Qt::red,5,Qt::SolidLine));
    p.drawLine(Frame.x1,Frame.y1,Frame.x2,Frame.y2);
}

void PainterClass::paintRect(FrameClass Frame, PropBox *ItemProps)
{
    // Start to paint in the buffer
    QPainter p(pScene->GetBuffer());
    p.setPen(QPen(Qt::red,2,Qt::SolidLine));
    p.drawRect(Frame.x1,Frame.y1,Frame.x2-Frame.x1,Frame.y2-Frame.y1);
}
