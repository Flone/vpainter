#pragma once
#include <QPainter>

class SceneClass;
class PropBox;
class FrameClass;

class PainterClass
{
public:

    PainterClass(SceneClass* pScene);

    void paintLine(FrameClass Frame, PropBox* ItemProps);

    void paintRect(FrameClass Frame, PropBox* ItemProps);

    void paintEllipse(FrameClass Frame, PropBox* ItemProps);

    void paintBorder(FrameClass Frame);

    void DrawSelectionPoint(int x, int y, int delta);

private:

    SceneClass* pScene;
};
