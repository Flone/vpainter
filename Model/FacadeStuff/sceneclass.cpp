#include "sceneclass.h"
#include "Controller/controllerclass.h"
#include "Model/FacadeStuff/storeclass.h"
#include "Model/GraphItemAndChilds/graphitem.h"
#include "Model/FacadeStuff/painterclass.h"
#include "Model/FacadeStuff/selectionstore.h"

#include "Model/GraphItemAndChilds/rectclass.h"
#include "Model/GraphItemAndChilds/lineclass.h"
#include "Model/GraphItemAndChilds/ellipseclass.h"

#include <typeinfo.h>
#include <thread>

#include <QPainter>
#include <QMouseEvent>

SceneClass::SceneClass(ControllerClass *pContoller, StoreClass *pStore,QWidget *parent) : QWidget(parent)
{
    this->pController = pContoller;
    this->pStore = pStore;

    // Now we will get mouseMoveEvent even when mouse button is not pressed
    setMouseTracking(true);

    bDrawOnFrontBuffer = true;

    // 1920x1080? Fix that.
    frontBuffer = new QPixmap(1920,1080);
    frontBuffer->fill(Qt::white);

    backBuffer  = new QPixmap(1920,1080);
    backBuffer->fill(Qt::white);
}

void SceneClass::Draw(PainterClass* pPainter)
{
    // Put stuff in buffer and draw it on screen later

    // Draw all items
    for (unsigned int i=0;i<pStore->Items.size();i++)
    {
        pStore->Items[i]->DrawItem(*pPainter);
    }

    // Draw some selections
    pStore->pSelectionStore->DrawSelections(*pPainter);

    // Update Scene
    repaint();
}

bool SceneClass::CheckHit(int x, int y)
{
    for (int i=pStore->Items.size()-1;i>=0;i--)
    {
        if (pStore->Items[i]->CheckHit(x,y))
        {
            unsigned char Type = 0;
            if (typeid(*pStore->Items[i]) == typeid(LineClass))
            {
                Type = 0;
            }
            else if (typeid(*pStore->Items[i]) == typeid(RectClass))
            {
                Type = 1;
            }
            else if (typeid(*pStore->Items[i]) == typeid(EllipseClass))
            {
                Type = 2;
            }
            else
            {
                Type = 3;
            }

            pStore->CreateSelection(pStore->Items[i],Type);
            return true;
        }
    }
    return false;
}

QPixmap *SceneClass::GetBuffer()
{
    if (bDrawOnFrontBuffer) return frontBuffer;
    else return backBuffer;
}

SceneClass::~SceneClass()
{
    delete frontBuffer;
    delete backBuffer;
}

void SceneClass::mousePressEvent(QMouseEvent *event)
{
    pController->SceneMouseDownOrUp(mapFromGlobal(QCursor::pos()).x(),mapFromGlobal(QCursor::pos()).y());
    event->ignore();
}

void SceneClass::mouseReleaseEvent(QMouseEvent *event)
{
    pController->SceneMouseDownOrUp(mapFromGlobal(QCursor::pos()).x(),mapFromGlobal(QCursor::pos()).y());
    event->ignore();
}

void SceneClass::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    if (bDrawOnFrontBuffer) painter.drawPixmap(0,0,*frontBuffer);
    else painter.drawPixmap(0,0,*backBuffer);

    // Switch buffer
    bDrawOnFrontBuffer = !bDrawOnFrontBuffer;

    std::thread clearThread(&SceneClass::clearBuffer,this);
    clearThread.join();
}

void SceneClass::mouseMoveEvent(QMouseEvent *event)
{
    pController->MouseMove(mapFromGlobal(QCursor::pos()).x(),mapFromGlobal(QCursor::pos()).y());
}

void SceneClass::clearBuffer()
{
    if (bDrawOnFrontBuffer)
    {
        frontBuffer->fill(Qt::white);
    }
    else
    {
        backBuffer->fill(Qt::white);
    }
}
