#pragma once

#include <QWidget>
#include <QPixmap>

class QMouseEvent;

class ControllerClass;
class PainterClass;
class StoreClass;
class QShortcut;

class SceneClass : public QWidget
{
public:

    SceneClass(ControllerClass *pContoller, StoreClass *pStore, QWidget *parent = nullptr);

    void Draw(PainterClass* pPainter);

    bool CheckHit(int x, int y);

    QPixmap *GetBuffer();

    ~SceneClass() override;

protected:

    virtual void mousePressEvent(QMouseEvent *event) override;

    virtual void mouseReleaseEvent(QMouseEvent *event) override;

    virtual void paintEvent(QPaintEvent *event) override;

    virtual void mouseMoveEvent(QMouseEvent *event) override;

private:

    void clearBuffer();

    QPixmap *frontBuffer;

    QPixmap *backBuffer;

    ControllerClass *pController;

    StoreClass      *pStore;

    bool bDrawOnFrontBuffer;
};
