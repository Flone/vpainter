#include "selectionstore.h"
#include "Model/SelectionClasses/selection.h"
#include "Model/FacadeStuff/painterclass.h"
#include "Model/SelectionClasses/groupselection.h"

#include <typeinfo.h>

SelectionStore::SelectionStore()
{
    GrabbedItem = nullptr;
}

void SelectionStore::AddSelection(Selection *pNewSelection)
{
    Objects.push_back(pNewSelection);
}

bool SelectionStore::TryGrab(int x, int y)
{
    for (unsigned int i=0;i<Objects.size();i++)
    {
        if (Objects[i]->TryGrab(x,y))
        {
            if (typeid(*Objects[i]) == typeid(GroupSelection))
            {
                GroupSelection* pGrabbed = (GroupSelection*)Objects[i];
                GrabbedItem = pGrabbed->GetGrabbedItem();
            }
            else GrabbedItem = Objects[i]->Item;
            return true;
        }
    }
    return false;
}

void SelectionStore::MoveTo(int x, int y)
{
    for (unsigned int i=0;i<Objects.size();i++)
    {
        Objects[i]->MoveTo(x,y);
    }
}

GraphItem *SelectionStore::GetGrabbedItem()
{
    return GrabbedItem;
}

void SelectionStore::DeleteSelectionWithItem(GraphItem *pItem)
{
    for (unsigned int i = 0; i < Objects.size(); i++)
    {
        if (Objects[i]->Item == pItem)
        {
            delete Objects[i];
            Objects.erase(Objects.begin()+i);
        }
    }
}

void SelectionStore::RecalculateSelection()
{
    if (Objects.size() != 0) Objects[0]->Recalculate();
}

void SelectionStore::DrawSelections(PainterClass PeterThePainter)
{
    if (Objects.size() != 0)
    {
        for (unsigned int i=0;i<Objects.size();i++)
        {
            Objects[i]->Draw(PeterThePainter);
        }
    }
}

void SelectionStore::DeleteAllSelections()
{
    if (Objects.size() != 0)
    {
        for (unsigned int i=0;i<Objects.size();i++)
        {
            delete Objects[i];
        }
        Objects.clear();
    }
}

SelectionStore::~SelectionStore()
{
    for (unsigned int i=0;i<Objects.size();i++)
    {
        delete Objects[i];
    }
}
