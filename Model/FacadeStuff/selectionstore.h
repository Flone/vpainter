#pragma once
#include <vector>

class Selection;
class PainterClass;
class GraphItem;

class SelectionStore
{
public:

    SelectionStore();

    void AddSelection(Selection* pNewSelection);

    bool TryGrab(int x, int y);

    void MoveTo(int x, int y);

    GraphItem *GetGrabbedItem();

    void DeleteSelectionWithItem(GraphItem* pItem);

    void RecalculateSelection();

    void DrawSelections(PainterClass PeterThePainter);

    void DeleteAllSelections();

    ~SelectionStore();

    std::vector<Selection*> Objects;

private:

    GraphItem* GrabbedItem;
};
