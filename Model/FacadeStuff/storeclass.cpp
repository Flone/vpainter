#include "storeclass.h"

#include "Model/GraphItemAndChilds/graphitem.h"
#include "Model/FacadeStuff/selectionstore.h"

#include "Model/GraphItemAndChilds/group.h"
#include "Model/GraphItemAndChilds/lineclass.h"
#include "Model/GraphItemAndChilds/ellipseclass.h"
#include "Model/GraphItemAndChilds/rectclass.h"

#include "Model/SelectionClasses/lineselection.h"
#include "Model/SelectionClasses/rectselection.h"
#include "Model/SelectionClasses/groupselection.h"

#include "Model/Commands/dragcommand.h"
#include "Model/Commands/groupcommand.h"

#include <typeinfo.h>

StoreClass::StoreClass()
{
    pSelectionStore = new SelectionStore();

    pLastGroup = nullptr;
}

void StoreClass::AddItem(GraphItem *pNewItem, unsigned char Type)
{
    Items.push_back(pNewItem);

    if (Type == 3)
    {
        std::vector<unsigned int> DeleteThis;

        // Erase the elements from the store that now in the group
        for (unsigned int i = 0; i < Items.size(); i++)
        {
            for (unsigned int j = 0; j < pSelectionStore->Objects.size(); j++)
            {
                if (pSelectionStore->Objects[j]->Item == Items[i])
                {
                    DeleteThis.push_back(i);
                    break;
                }
            }
        }

        std::vector<GraphItem*> TempItems;
        for (unsigned int i = 0; i < Items.size(); i++)
        {
            bool Skip = false;
            for (unsigned int j = 0; j < DeleteThis.size(); j++)
            {
                if (i == DeleteThis[j])
                {
                    Skip = true;
                    break;
                }
            }
            if (Skip) continue;
            else TempItems.push_back(Items[i]);
        }

        Items = TempItems;
    }

    // Clear elements selections, later we will user group selection
    DeleteAllSelections();

    CreateSelection(pNewItem, Type);
}

void StoreClass::MoveTo(int x, int y)
{
    pSelectionStore->MoveTo(x,y);
}

bool StoreClass::TryGrab(int x, int y)
{
    if (pSelectionStore->TryGrab(x,y)) return true;
    else return false;
}

void StoreClass::AddCommand(Command *pCommand)
{
    if (CommandStack.size() > 300)
    {
        delete CommandStack[0];
        CommandStack.erase(CommandStack.begin());
    }

    CommandStack.push_back(pCommand);
}

void StoreClass::Undo()
{
    if (CommandStack.size() != 0)
    {
        CommandStack[CommandStack.size() - 1]->Undo();
        delete CommandStack[CommandStack.size() - 1];
        CommandStack.pop_back();
    }
}

void StoreClass::DeleteElement(GraphItem *pElement)
{
    // Delete element's selection if it's exists
    pSelectionStore->DeleteSelectionWithItem(pElement);

    for (unsigned int i = 0; i < Items.size(); i++)
    {
        if (Items[i] == pElement)
        {
            delete Items[i];
            Items.erase(Items.begin()+i);
        }
    }
}

GraphItem *StoreClass::GetLastElement()
{
    return Items[Items.size() - 1];
}

std::vector<Selection *>& StoreClass::ReturnAllSelections()
{
    return pSelectionStore->Objects;
}

void StoreClass::UnGroup()
{
    GroupSelection* pGroup = nullptr;
    for (unsigned int i = 0; i < pSelectionStore->Objects.size(); i++)
    {
        if (typeid(*pSelectionStore->Objects[i]) == typeid(GroupSelection))
            pGroup = (GroupSelection*)pSelectionStore->Objects[i];
    }

    if (pGroup != nullptr)
    {
        pLastGroup = pGroup->Item;

        // Figure out what is group index in Items massive
        int GroupIndex = 0;

        for (unsigned int i = 0; i < Items.size(); i++)
        {
            if (Items[i] == pGroup->Item) GroupIndex = i;
        }

        std::vector<GraphItem*> TempItems;
        // Erase this index from Items
        for (unsigned int i = 0; i < Items.size(); i++)
        {
            if (i != GroupIndex)
            {
                TempItems.push_back(Items[i]);
            }
        }
        Items = TempItems;

        Group* pG = (Group*)pGroup->Item;

        for (unsigned int i = 0; i < pG->GroupItems.size(); i++)
        {
            Items.push_back(pG->GroupItems[i]);
        }
    }
    else pLastGroup = nullptr;
}

void StoreClass::UnGroupByItem(GraphItem *pGroup)
{
    // Delete group's selection
    pSelectionStore->DeleteSelectionWithItem(pGroup);

    // Erase group from Store
    for (unsigned int i = 0; i < Items.size(); i++)
    {
        if (Items[i] == pGroup)
        {
            Items.erase(Items.begin() + i);
            break;
        }
    }

    // Add group elements to Store
    Group* pG = (Group*)pGroup;

    for (unsigned int i = 0; i < pG->GroupItems.size(); i++)
    {
        Items.push_back(pG->GroupItems[i]);
    }

    pG->GroupItems.clear();

    delete pG;
}

void StoreClass::DeleteAllSelections()
{
    pSelectionStore->DeleteAllSelections();
}

GraphItem *StoreClass::GetGrabbedItem()
{
    return pSelectionStore->GetGrabbedItem();
}

void StoreClass::CreateSelection(GraphItem *pNewItem, unsigned char Type)
{
    for (unsigned int i = 0; i < pSelectionStore->Objects.size(); i++)
    {
        if (pSelectionStore->Objects[i]->Item == pNewItem)
        {
            // This item already has the selection
            return;
        }
    }

    pSelectionStore->AddSelection(pNewItem->ReturnSelection());
}

StoreClass::~StoreClass()
{
    if (CommandStack.size() != 0)
    {
        for (int i = CommandStack.size() - 1; i >= 0; i--)
        {
            delete CommandStack[i];
            CommandStack.pop_back();
        }
    }

    delete pSelectionStore;

    for (unsigned int i = 0; i < Items.size(); i++)
    {
        delete Items[i];
    }
}

void StoreClass::GroupUndo(GraphItem *pGroup)
{
    Group* pG = (Group*)pGroup;
    for (unsigned int i = 0; i < pG->GroupItems.size(); i++)
    {
        Items.pop_back();
    }

    Items.push_back(pG);
}

GraphItem *StoreClass::GetLastGroup()
{
    return pLastGroup;
}

void StoreClass::RecalculateSelections()
{
    pSelectionStore->RecalculateSelection();
}
