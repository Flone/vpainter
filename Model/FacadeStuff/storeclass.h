#pragma once
#include <vector>
#include "Model/Commands/command.h"

class GraphItem;
class SelectionStore;
class Selection;
class Command;

class StoreClass
{
public:

    StoreClass();

    void AddItem(GraphItem* pNewItem, unsigned char Type);

    void MoveTo(int x, int y);

    bool TryGrab(int x, int y);

    void AddCommand(Command* pCommand);

    void Undo();

    void DeleteElement(GraphItem* pElement);

    void GroupUndo(GraphItem* pGroup);

    GraphItem *GetLastGroup();

    void RecalculateSelections();

    GraphItem *GetLastElement();

    std::vector<Selection *>& ReturnAllSelections();

    void UnGroup();

    void UnGroupByItem(GraphItem *pGroup);

    void CreateSelection(GraphItem *pNewItem, unsigned char Type);

    void DeleteAllSelections();

    GraphItem *GetGrabbedItem();

    ~StoreClass();

    std::vector<GraphItem*> Items;

    SelectionStore* pSelectionStore;

private:

    GraphItem* pLastGroup;

    std::vector<Command*> CommandStack;
};
