#include "atomgraphitem.h"
#include "Model/frameclass.h"
#include "Model/PropsClasses/propbox.h"

AtomGraphItem::AtomGraphItem(FrameClass Frame, PropBox *pNewProps)
{
    FrameObj = Frame;
    ItemProps = pNewProps;
}

AtomGraphItem::AtomGraphItem(const AtomGraphItem &CopyThis) : GraphItem (CopyThis)
{
    this->ItemProps = new PropBox(*CopyThis.ItemProps);
}

void AtomGraphItem::ChangeProps(PropBox *pNewProps)
{
    delete ItemProps;
    ItemProps = pNewProps;
}

AtomGraphItem::~AtomGraphItem()
{
    delete ItemProps;
}
