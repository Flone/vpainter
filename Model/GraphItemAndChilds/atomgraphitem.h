#pragma once
#include "Model/GraphItemAndChilds/graphitem.h"

class FrameClass;
class PropBox;

class AtomGraphItem : public GraphItem
{
public:
    AtomGraphItem(FrameClass Frame, PropBox* pNewProps);

    AtomGraphItem(const AtomGraphItem& CopyThis);

    void ChangeProps(PropBox* pNewProps);

    virtual ~AtomGraphItem() override;

protected:

    PropBox* ItemProps;
};
