#include "ellipseclass.h"
#include "Model/frameclass.h"
#include "Model/PropsClasses/propbox.h"
#include "Model/FacadeStuff/painterclass.h"

#include "Model/SelectionClasses/rectselection.h"

EllipseClass::EllipseClass(FrameClass FrameObject1, PropBox *NewProps) : AtomGraphItem (FrameObject1, NewProps)
{
}

EllipseClass::EllipseClass(const EllipseClass &CopyThis) : AtomGraphItem (CopyThis)
{
}

void EllipseClass::DrawItem(PainterClass PeterThePainter)
{
    PeterThePainter.paintEllipse(FrameObj, ItemProps);
}

bool EllipseClass::CheckHit(int x, int y)
{
    //(x^2/a^2) + (y^2/b^2) <= 1

    int xLeft = 0;
    int xRight = 0;
    int yTop = 0;
    int yDown = 0;

    // Set xLeft & xRight
    if (FrameObj.x1 < FrameObj.x2)
    {
        xLeft = FrameObj.x1;
        xRight = FrameObj.x2;
    }
    else
    {
        xLeft = FrameObj.x2;
        xRight = FrameObj.x1;
    }

    // Set yTop & yDown
    if (FrameObj.y1 < FrameObj.y2)
    {
        yTop = FrameObj.y1;
        yDown = FrameObj.y2;
    }
    else
    {
        yTop = FrameObj.y2;
        yDown = FrameObj.y1;
    }

    // Calculate a and b
    int a = (xRight - xLeft)/2;
    int b = (yDown - yTop)/2;

    //Calculate local x and y (relative to the center of the ellipse)
    int LocalX = x - xLeft - a;
    int LocalY = y - yTop - b;

    if ( ( (pow(LocalX,2) / pow(a,2)) + (pow(LocalY,2) / pow(b,2) ) <= 1) )
    return true;

    return false;

}

Selection *EllipseClass::ReturnSelection()
{
    // Rect selection
    RectSelection* pRectSelection = new RectSelection(this);
    return pRectSelection;
}

void EllipseClass::SetFrame(FrameClass FrameObj)
{
    this->FrameObj = FrameObj;
}
