#pragma once
#include "Model/GraphItemAndChilds/atomgraphitem.h"

class PainterClass;
class FrameClass;
class PropBox;
class Selection;

class EllipseClass : public AtomGraphItem
{
public:

    EllipseClass(FrameClass FrameObject1, PropBox* NewProps);

    EllipseClass(const EllipseClass& CopyThis);

    virtual void DrawItem(PainterClass PeterThePainter) override;

    virtual bool CheckHit(int x, int y) override;

    virtual Selection *ReturnSelection() override;

    virtual void SetFrame(FrameClass FrameObj) override;

    virtual ~EllipseClass() override{}
};
