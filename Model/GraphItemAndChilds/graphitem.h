#pragma once
#include "Model/frameclass.h"

class PainterClass;
class Selection;

class GraphItem
{
public:

    GraphItem();

    GraphItem(const GraphItem& CopyThis);

    virtual void DrawItem(PainterClass PeterThePainter) = 0;

    virtual bool CheckHit(int x, int y) = 0;

    virtual Selection* ReturnSelection() = 0;

    virtual ~GraphItem();

    virtual void SetFrame(FrameClass FrameObj) = 0;

    FrameClass FrameObj;
};
