#include "group.h"
#include "Model/FacadeStuff/painterclass.h"

#include "Model/SelectionClasses/groupselection.h"
#include "Model/GraphItemAndChilds/ellipseclass.h"
#include "Model/GraphItemAndChilds/lineclass.h"
#include "Model/GraphItemAndChilds/rectclass.h"

#include <typeinfo.h>

Group::Group()
{

}

Group::Group(const Group &CopyThis)
{
    this->FrameObj = CopyThis.FrameObj;

    for (unsigned int i = 0; i < CopyThis.GroupItems.size(); i++)
    {
        GraphItem* pNewElement = nullptr;
        if (typeid(*CopyThis.GroupItems[i]) == typeid(LineClass))
        {
            LineClass* pLine = (LineClass*)CopyThis.GroupItems[i];
            pNewElement = new LineClass(*pLine);
        }
        else if (typeid(*CopyThis.GroupItems[i]) == typeid(EllipseClass))
        {
            EllipseClass* pEll = (EllipseClass*)CopyThis.GroupItems[i];
            pNewElement = new EllipseClass(*pEll);
        }
        else if (typeid(*CopyThis.GroupItems[i]) == typeid(RectClass))
        {
            RectClass* pRect = (RectClass*)CopyThis.GroupItems[i];
            pNewElement = new RectClass(*pRect);
        }
        else
        {
            Group* pGr = (Group*)CopyThis.GroupItems[i];
            pNewElement = new Group(*pGr);
        }
        GroupItems.push_back(pNewElement);
    }
}

void Group::AddToGroup(GraphItem *NewItem)
{
    GroupItems.push_back(NewItem);
    if (GroupItems.size() > 1)
    {
       FrameObj = FrameObj.Compose(NewItem->FrameObj);
    }
    else
    {
        FrameObj = NewItem->FrameObj;
    }
}

void Group::DrawItem(PainterClass PeterThePainter)
{
    for (unsigned int i = 0; i < GroupItems.size(); i++)
    {
        GroupItems[i]->DrawItem(PeterThePainter);
    }
}

bool Group::CheckHit(int x, int y)
{
    for (unsigned int i = 0; i < GroupItems.size(); i++)
    {
        if (GroupItems[i]->CheckHit(x,y)) return true;
    }
    return false;
}

void Group::EraseFromGroup(GraphItem *pEraseThis)
{
    for (unsigned int i = 0; i < GroupItems.size(); i++)
    {
        if (GroupItems[i] == pEraseThis)
        {
            GroupItems.erase(GroupItems.begin() + i);
            break;
        }
    }
}

Selection* Group::ReturnSelection()
{
    GroupSelection* pGroupSelection = new GroupSelection(this);
    for (unsigned int i = 0; i < GroupItems.size(); i++)
    {
        pGroupSelection->AddSelection(GroupItems[i]->ReturnSelection());
    }
    pGroupSelection->Init();
    return pGroupSelection;
}

void Group::SetFrame(FrameClass FrameObj)
{
    int deltaX = FrameObj.x1 - this->FrameObj.x1;
    int deltaY = FrameObj.y1 - this->FrameObj.y1;

    this->FrameObj = FrameObj;

    for (unsigned int i = 0; i < GroupItems.size(); i++)
    {
        FrameClass Frame;
        Frame.x1 = GroupItems[i]->FrameObj.x1 + deltaX;
        Frame.x2 = GroupItems[i]->FrameObj.x2 + deltaX;
        Frame.y1 = GroupItems[i]->FrameObj.y1 + deltaY;
        Frame.y2 = GroupItems[i]->FrameObj.y2 + deltaY;

        // Here we will call this (Group::SetFrame) method if this group has another group as a child
        GroupItems[i]->SetFrame(Frame);
    }
}

Group::~Group()
{
    for (unsigned int i = 0; i < GroupItems.size(); i++)
    {
        delete GroupItems[i];
    }
}
