#pragma once
#include <vector>

#include "Model/GraphItemAndChilds/graphitem.h"

class PainterClass;
class Selection;

class Group : public GraphItem
{
public:

    Group();

    Group(const Group& CopyThis);

    void AddToGroup(GraphItem* NewItem);

    virtual void DrawItem(PainterClass PeterThePainter) override;

    virtual bool CheckHit(int x, int y) override;

    void EraseFromGroup(GraphItem* pEraseThis);

    virtual Selection* ReturnSelection() override;

    virtual void SetFrame(FrameClass FrameObj) override;

    virtual ~Group() override;

    std::vector<GraphItem*> GroupItems;
};
