#include "lineclass.h"
#include "Model/frameclass.h"
#include "Model/PropsClasses/propbox.h"
#include "Model/FacadeStuff/painterclass.h"

#include "Model/SelectionClasses/lineselection.h"

LineClass::LineClass(FrameClass FrameObject1, PropBox *NewProps) : AtomGraphItem (FrameObject1,NewProps)
{
}

LineClass::LineClass(const LineClass &CopyThis) : AtomGraphItem (CopyThis)
{
}

void LineClass::DrawItem(PainterClass PeterThePainter)
{
    PeterThePainter.paintLine(FrameObj,ItemProps);
}

bool LineClass::CheckHit(int x, int y)
{
    // Line equation: y = kx + b

    int maxX = FrameObj.x1 > FrameObj.x2 ? FrameObj.x1 : FrameObj.x2;
    int maxY = FrameObj.y1 > FrameObj.y2 ? FrameObj.y1 : FrameObj.y2;
    int minX = FrameObj.x1 < FrameObj.x2 ? FrameObj.x1 : FrameObj.x2;
    int minY = FrameObj.y1 < FrameObj.y2 ? FrameObj.y1 : FrameObj.y2;


    // Calculate k
    float k = 0;
    // If we got vertical line
    if (FrameObj.x2 == FrameObj.x1)
    {
        k = 1;
    }
    // If we got horizontal line
    else if (FrameObj.y2 == FrameObj.y1) k = 1;
    else k = float(FrameObj.y2 - FrameObj.y1) / (FrameObj.x2 - FrameObj.x1);


    // Calculate b
    float b = 0;
    if (k == 1.0f) b = minY;
    else b = FrameObj.y2 - FrameObj.x2 * k;


    // Hit error = Hit +- eps
    int eps = 30;


    int CalculatedY = int(k * x + b);


    // Check if we are in object range
    if ((x >= (minX - eps)) && (x <= (maxX + eps)) && (y >= (minY - eps)) && (y <= (maxY + eps)))
    {
        if ( (y >= (CalculatedY - eps)) && (y <= (CalculatedY + eps)) )
        {
            return true;
        }
    }
    return false;
}

Selection* LineClass::ReturnSelection()
{
    // Line selection
    LineSelection* pLineSelection = new LineSelection(this);
    return pLineSelection;
}

void LineClass::SetFrame(FrameClass FrameObj)
{
    this->FrameObj = FrameObj;
}
