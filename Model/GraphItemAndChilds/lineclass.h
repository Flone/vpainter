#pragma once
#include "Model/GraphItemAndChilds/atomgraphitem.h"

class PainterClass;
class FrameClass;
class PropBox;
class Selection;

class LineClass : public AtomGraphItem
{
public:

    LineClass(FrameClass FrameObject1, PropBox* NewProps);

    LineClass(const LineClass& CopyThis);

    virtual void DrawItem(PainterClass PeterThePainter) override;

    virtual bool CheckHit(int x, int y) override;

    virtual Selection* ReturnSelection() override;

    virtual void SetFrame(FrameClass FrameObj) override;

    virtual ~LineClass() override{}
};
