#include "rectclass.h"
#include "Model/frameclass.h"
#include "Model/PropsClasses/propbox.h"
#include "Model/FacadeStuff/painterclass.h"

#include "Model/SelectionClasses/rectselection.h"

RectClass::RectClass(FrameClass FrameObject1, PropBox *NewProps) : AtomGraphItem (FrameObject1,NewProps)
{
}

RectClass::RectClass(const RectClass &CopyThis) : AtomGraphItem (CopyThis)
{
}

void RectClass::DrawItem(PainterClass PeterThePainter)
{
    PeterThePainter.paintRect(FrameObj, ItemProps);
}

bool RectClass::CheckHit(int x, int y)
{
    int xLeft = 0;
    int xRight = 0;
    int yTop = 0;
    int yDown = 0;

    // Set xLeft & xRight
    if (FrameObj.x1 < FrameObj.x2)
    {
        xLeft = FrameObj.x1;
        xRight = FrameObj.x2;
    }
    else
    {
        xLeft = FrameObj.x2;
        xRight = FrameObj.x1;
    }

    // Set yTop & yDown
    if (FrameObj.y1 < FrameObj.y2)
    {
        yTop = FrameObj.y1;
        yDown = FrameObj.y2;
    }
    else
    {
        yTop = FrameObj.y2;
        yDown = FrameObj.y1;
    }

    // Check hit
    if ( (x >= xLeft) && (x <= xRight) && (y >= yTop) && (y <= yDown) ) return true;
    else return false;
}

Selection *RectClass::ReturnSelection()
{
    // Rect selection
    RectSelection* pRectSelection = new RectSelection(this);
    return pRectSelection;
}

void RectClass::SetFrame(FrameClass FrameObj)
{
    this->FrameObj = FrameObj;
}
