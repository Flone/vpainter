#pragma once
#include "Model/GraphItemAndChilds/atomgraphitem.h"

class PainterClass;
class FrameClass;
class PropBox;
class Selection;

class RectClass : public AtomGraphItem
{
public:

    RectClass(FrameClass FrameObject1, PropBox* NewProps);

    RectClass(const RectClass& CopyThis);

    virtual void DrawItem(PainterClass PeterThePainter) override;

    virtual bool CheckHit(int x, int y) override;

    virtual Selection *ReturnSelection() override;

    virtual void SetFrame(FrameClass FrameObj) override;

    virtual ~RectClass() override{}
};
