#pragma once
#include "Model/PropsClasses/props.h"

class BrushProp : public Props
{
public:
    // Brush is the fill color
    unsigned char BrushRColor;
    unsigned char BrushGColor;
    unsigned char BrushBColor;
};
