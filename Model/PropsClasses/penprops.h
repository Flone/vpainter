#pragma once
#include "Model/PropsClasses/props.h"

class PenProp : public Props
{
public:

    // Pen is the outline
    unsigned char PenRColor;
    unsigned char PenGColor;
    unsigned char PenBColor;
};
