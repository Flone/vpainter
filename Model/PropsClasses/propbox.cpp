#include "propbox.h"

PropBox::PropBox()
{

}

PropBox::PropBox(const PropBox &CopyThis)
{
    PropPen = CopyThis.PropPen;
    PropBrush = CopyThis.PropBrush;
}

void PropBox::setParams(PenProp PenP, BrushProp BrushP)
{
    // Set new props
    PropPen   = PenP;
    PropBrush = BrushP;
}

PenProp PropBox::GetPenProp()
{
    return PropPen;
}

BrushProp PropBox::GetBrushProp()
{
    return PropBrush;
}
