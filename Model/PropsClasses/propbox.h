#pragma once
#include "Model/PropsClasses/brushprop.h"
#include "Model/PropsClasses/penprops.h"

class PropBox
{
public:

    PropBox();

    PropBox(const PropBox& CopyThis);

    void setParams(PenProp PenP, BrushProp BrushP);

    PenProp GetPenProp();

    BrushProp GetBrushProp();

private:
    // Some props
    PenProp   PropPen;
    BrushProp PropBrush;
};
