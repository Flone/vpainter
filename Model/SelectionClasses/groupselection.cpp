#include "groupselection.h"
#include "Model/GraphItemAndChilds/graphitem.h"
#include "Model/GraphItemAndChilds/group.h"
#include "Model/FacadeStuff/painterclass.h"

#include "Model/SelectionClasses/selection.h"

#include "Model/PropsClasses/propbox.h"

#include <typeinfo.h>

GroupSelection::GroupSelection(GraphItem* Item)  : Selection (Item)
{
    // We don't put logic here because when GroupSelection is being created then it does not have all ChildSelections
    // in it yet
    LastGrabbedItem = nullptr;
}

void GroupSelection::Init()
{
    CentralPointGrabbed = false;

    int LeftX   = ChildsSelections[0]->Item->FrameObj.LeftX();
    int RightX  = ChildsSelections[0]->Item->FrameObj.RightX();
    int TopY    = ChildsSelections[0]->Item->FrameObj.TopY();
    int BottomY = ChildsSelections[0]->Item->FrameObj.BottomY();

    // Calculate borders
    for (unsigned int i = 1; i < ChildsSelections.size(); i++)
    {
        if (LeftX > ChildsSelections[i]->Item->FrameObj.LeftX()) LeftX = ChildsSelections[i]->Item->FrameObj.LeftX();
        if (RightX < ChildsSelections[i]->Item->FrameObj.RightX()) RightX = ChildsSelections[i]->Item->FrameObj.RightX();
        if (TopY > ChildsSelections[i]->Item->FrameObj.TopY()) TopY = ChildsSelections[i]->Item->FrameObj.TopY();
        if (BottomY < ChildsSelections[i]->Item->FrameObj.BottomY()) BottomY = ChildsSelections[i]->Item->FrameObj.BottomY();
    }

    x1 = LeftX;
    y1 = TopY;

    x2 = RightX;
    y2 = TopY;

    x3 = LeftX;
    y3 = BottomY;

    x4 = RightX;
    y4 = BottomY;

    CentralX = x1 + (x4 - x1)/2;
    CentralY = y1 + (y4 - y1)/2;

    // Update Group frame
    Item->FrameObj.x1 = x1;
    Item->FrameObj.x2 = x4;
    Item->FrameObj.y1 = y1;
    Item->FrameObj.y2 = y4;
}

bool GroupSelection::TryGrab(int x, int y)
{
    int delta = 12;

    // Check if user is trying to grab central point
    if ( (x <= (CentralX + delta)) && (x >= (CentralX - delta)) )
    {
        // x axis is correct
        if ( (y <= (CentralY + delta)) && (y >= (CentralY - delta)) )
        {
            // y axis is correct too
            CentralPointGrabbed = true;

            // Change grabbed point to all childs
            for (unsigned int i = 0; i < ChildsSelections.size(); i++)
            {
                ChildsSelections[i]->ChangeGrabbedPoint(0);
            }

            LastGrabbedItem = Item;
            return true;
        }
    }

    // Maybe user want to grab a child?

    // Change grabbed point to NONE
    for (unsigned int i = 0; i < ChildsSelections.size(); i++)
    {
        ChildsSelections[i]->ChangeGrabbedPoint(5);
    }

    CentralPointGrabbed = false;

    bool Result = false;

    for (unsigned int i = 0; i < ChildsSelections.size(); i++)
    {
        if (ChildsSelections[i]->TryGrab(x,y))
        {
            if (typeid(*ChildsSelections[i]) == typeid(GroupSelection))
            {
                GroupSelection* pG = (GroupSelection*)ChildsSelections[i];
                LastGrabbedItem = pG->GetGrabbedItem();
                if (LastGrabbedItem != nullptr)
                {
                    Result = true;
                    break;
                }
            }
            else
            {
                Result = true;
                LastGrabbedItem = ChildsSelections[i]->Item;
                break;
            }
        }
    }

    if (!Result) LastGrabbedItem = nullptr;

    return Result;
}

void GroupSelection::Draw(PainterClass PeterThePainter)
{
    int delta = 20;

    for (unsigned int i = 0; i < ChildsSelections.size(); i++)
    {
        ChildsSelections[i]->Draw(PeterThePainter);
    }

    FrameClass BorderFrame;
    BorderFrame.x1 = x1;
    BorderFrame.y1 = y1;
    BorderFrame.x2 = x2;
    BorderFrame.y2 = y4;

    PeterThePainter.paintBorder(BorderFrame);

    PeterThePainter.DrawSelectionPoint(CentralX,CentralY,delta);
}

void GroupSelection::MoveTo(int x, int y, bool Local)
{
    if (Local)
    {
        Item->FrameObj.x1 += x;
        Item->FrameObj.x2 += x;
        Item->FrameObj.y1 += y;
        Item->FrameObj.y2 += y;

        x1 += x;
        y1 += y;
        x2 += x;
        y2 += y;
        x3 += x;
        y3 += y;
        x4 += x;
        y4 += y;

        for (unsigned int i = 0; i < ChildsSelections.size(); i++)
        {
            ChildsSelections[i]->MoveTo(x,y,true);
        }
    }
    else if (CentralPointGrabbed)
    {
        // Move all childs
        int LocalShiftX = x - CentralX;
        int LocalShiftY = y - CentralY;
        for (unsigned int i = 0; i < ChildsSelections.size(); i++)
        {
            ChildsSelections[i]->MoveTo(LocalShiftX,LocalShiftY,true);
        }

        Item->FrameObj.x1 += x - CentralX;
        Item->FrameObj.y1 += y - CentralY;
        Item->FrameObj.x2 += x - CentralX;
        Item->FrameObj.y2 += y - CentralY;

        x1 += x - CentralX;
        y1 += y - CentralY;
        x2 += x - CentralX;
        y2 += y - CentralY;
        x3 += x - CentralX;
        y3 += y - CentralY;
        x4 += x - CentralX;
        y4 += y - CentralY;
    }
    else
    {
        for (unsigned int i = 0; i < ChildsSelections.size(); i++)
        {
            ChildsSelections[i]->MoveTo(x,y);
        }
    }

    int LeftX   = ChildsSelections[0]->Item->FrameObj.LeftX();
    int RightX  = ChildsSelections[0]->Item->FrameObj.RightX();
    int TopY    = ChildsSelections[0]->Item->FrameObj.TopY();
    int BottomY = ChildsSelections[0]->Item->FrameObj.BottomY();

    // Calculate borders
    for (unsigned int i = 1; i < ChildsSelections.size(); i++)
    {
        if (LeftX > ChildsSelections[i]->Item->FrameObj.LeftX()) LeftX = ChildsSelections[i]->Item->FrameObj.LeftX();
        if (RightX < ChildsSelections[i]->Item->FrameObj.RightX()) RightX = ChildsSelections[i]->Item->FrameObj.RightX();
        if (TopY > ChildsSelections[i]->Item->FrameObj.TopY()) TopY = ChildsSelections[i]->Item->FrameObj.TopY();
        if (BottomY < ChildsSelections[i]->Item->FrameObj.BottomY()) BottomY = ChildsSelections[i]->Item->FrameObj.BottomY();
    }

    x1 = LeftX;
    y1 = TopY;

    x2 = RightX;
    y2 = TopY;

    x3 = LeftX;
    y3 = BottomY;

    x4 = RightX;
    y4 = BottomY;

    CentralX = x1 + (x4 - x1)/2;
    CentralY = y1 + (y4 - y1)/2;

    // Update Group frame
    Item->FrameObj.x1 = x1;
    Item->FrameObj.x2 = x4;
    Item->FrameObj.y1 = y1;
    Item->FrameObj.y2 = y4;
}

void GroupSelection::ChangeGrabbedPoint(unsigned char NewPoint)
{
    switch(NewPoint)
    {
    case(0):
        CentralPointGrabbed = true;
        break;
    case(1):
        CentralPointGrabbed = false;
        break;
    case(2):
        CentralPointGrabbed = false;
        break;
    case(3):
        CentralPointGrabbed = false;
        break;
    case(4):
        CentralPointGrabbed = false;
        break;
    case(5):
        CentralPointGrabbed = false;
        break;
    }
}

GroupSelection::~GroupSelection()
{
    for (unsigned int i = 0; i < ChildsSelections.size(); i++)
    {
        delete ChildsSelections[i];
    }
    ChildsSelections.clear();
}

void GroupSelection::Recalculate()
{
    for (unsigned int i = 0; i < ChildsSelections.size(); i++)
    {
        ChildsSelections[i]->Recalculate();
    }

    int LeftX   = ChildsSelections[0]->Item->FrameObj.LeftX();
    int RightX  = ChildsSelections[0]->Item->FrameObj.RightX();
    int TopY    = ChildsSelections[0]->Item->FrameObj.TopY();
    int BottomY = ChildsSelections[0]->Item->FrameObj.BottomY();

    // Calculate borders
    for (unsigned int i = 1; i < ChildsSelections.size(); i++)
    {
        if (LeftX > ChildsSelections[i]->Item->FrameObj.LeftX()) LeftX = ChildsSelections[i]->Item->FrameObj.LeftX();
        if (RightX < ChildsSelections[i]->Item->FrameObj.RightX()) RightX = ChildsSelections[i]->Item->FrameObj.RightX();
        if (TopY > ChildsSelections[i]->Item->FrameObj.TopY()) TopY = ChildsSelections[i]->Item->FrameObj.TopY();
        if (BottomY < ChildsSelections[i]->Item->FrameObj.BottomY()) BottomY = ChildsSelections[i]->Item->FrameObj.BottomY();
    }

    x1 = LeftX;
    y1 = TopY;

    x2 = RightX;
    y2 = TopY;

    x3 = LeftX;
    y3 = BottomY;

    x4 = RightX;
    y4 = BottomY;

    CentralX = x1 + (x4 - x1)/2;
    CentralY = y1 + (y4 - y1)/2;

    // Update Group frame
    Item->FrameObj.x1 = x1;
    Item->FrameObj.x2 = x4;
    Item->FrameObj.y1 = y1;
    Item->FrameObj.y2 = y4;
}

GraphItem *GroupSelection::GetGrabbedItem()
{
    return LastGrabbedItem;
}

void GroupSelection::AddSelection(Selection *pSelection)
{
    ChildsSelections.push_back(pSelection);
}
