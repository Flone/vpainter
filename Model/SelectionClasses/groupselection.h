#pragma once
#include "Model/SelectionClasses/selection.h"
#include <vector>

class GraphItem;
class PainterClass;

class GroupSelection : public Selection
{
public:
    // Group goes here
    GroupSelection(GraphItem* Item);

    void Init();

    virtual bool TryGrab(int x, int y) override;

    virtual void Draw(PainterClass PeterThePainter) override;

    virtual void MoveTo(int x, int y, bool Local = false) override;

    virtual void ChangeGrabbedPoint (unsigned char NewPoint) override;

    virtual ~GroupSelection() override;

    virtual void Recalculate() override;

    GraphItem *GetGrabbedItem();

    void AddSelection(Selection* pSelection);

    // First point for which you can change the size
    int x1;
    int y1;

    // Second point for which you can change the size
    int x2;
    int y2;

    // Third point for which you can change the size
    int x3;
    int y3;

    // Fourth point for which you can change the size
    int x4;
    int y4;

    std::vector<Selection*> ChildsSelections;

private:

    GraphItem* LastGrabbedItem;
};
