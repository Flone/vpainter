#include "lineselection.h"
#include "Model/GraphItemAndChilds/graphitem.h"
#include "Model/FacadeStuff/painterclass.h"

LineSelection::LineSelection(GraphItem* Item) : Selection (Item)
{
    FirstPointGrabbed   = false;
    SecondPointGrabbed  = false;
    CentralPointGrabbed = false;

    x1 = Item->FrameObj.x1;
    y1 = Item->FrameObj.y1;

    x2 = Item->FrameObj.x2;
    y2 = Item->FrameObj.y2;

    CentralX = x1 + (x2 - x1)/2;
    CentralY = y1 + (y2 - y1)/2;
}

bool LineSelection::TryGrab(int x, int y)
{
    int delta = 12;

    // Check if user is trying to grab first point
    if ( (x <= (x1 + delta)) && (x >= (x1 - delta)) )
    {
        // x axis is correct
        if ( (y <= (y1 + delta)) && (y >= (y1 - delta)) )
        {
            // y axis is correct too
            FirstPointGrabbed = true;
            SecondPointGrabbed = false;
            CentralPointGrabbed = false;
            return true;
        }
    }
    // Check if user is trying to grab second point
    if ( (x <= (x2 + delta)) && (x >= (x2 - delta)) )
    {
        // x axis is correct
        if ( (y <= (y2 + delta)) && (y >= (y2 - delta)) )
        {
            // y axis is correct too
            SecondPointGrabbed = true;
            FirstPointGrabbed = false;
            CentralPointGrabbed = false;
            return true;
        }
    }
    // Check if user is trying to grab central point
    if ( (x <= (CentralX + delta)) && (x >= (CentralX - delta)) )
    {
        // x axis is correct
        if ( (y <= (CentralY + delta)) && (y >= (CentralY - delta)) )
        {
            // y axis is correct too
            SecondPointGrabbed = false;
            FirstPointGrabbed = false;
            CentralPointGrabbed = true;
            return true;
        }
    }
    CentralPointGrabbed = false;
    FirstPointGrabbed = false;
    SecondPointGrabbed = false;
    return false;
}

void LineSelection::Draw(PainterClass PeterThePainter)
{
    int delta = 20;

    PeterThePainter.DrawSelectionPoint(x1,y1,delta);
    PeterThePainter.DrawSelectionPoint(x2,y2,delta);
    PeterThePainter.DrawSelectionPoint(CentralX,CentralY,delta);
}

void LineSelection::MoveTo(int x, int y, bool Local)
{
    if (Local == true)
    {
        // This element is in a group and this command came from group master
        // x and y is a local shift from GrabbedPoint
        Item->FrameObj.x1 += x;
        Item->FrameObj.y1 += y;
        Item->FrameObj.x2 += x;
        Item->FrameObj.y2 += y;

        x1 += x;
        y1 += y;
        x2 += x;
        y2 += y;
    }
    else if (FirstPointGrabbed)
    {
        Item->FrameObj.x1 = x;
        Item->FrameObj.y1 = y;

        x1 = x;
        y1 = y;
    }
    else if (SecondPointGrabbed)
    {
        Item->FrameObj.x2 = x;
        Item->FrameObj.y2 = y;

        x2 = x;
        y2 = y;
    }
    else if (CentralPointGrabbed)
    {
        Item->FrameObj.x1 += x - CentralX;
        Item->FrameObj.y1 += y - CentralY;
        Item->FrameObj.x2 += x - CentralX;
        Item->FrameObj.y2 += y - CentralY;

        x1 += x - CentralX;
        y1 += y - CentralY;
        x2 += x - CentralX;
        y2 += y - CentralY;
    }

    CentralX = x1 + (x2 - x1)/2;
    CentralY = y1 + (y2 - y1)/2;
}

void LineSelection::ChangeGrabbedPoint(unsigned char NewPoint)
{
    switch(NewPoint)
    {
    case(0):
        CentralPointGrabbed = true;
        FirstPointGrabbed   = false;
        SecondPointGrabbed  = false;
        break;
    case(1):
        CentralPointGrabbed = false;
        FirstPointGrabbed   = true;
        SecondPointGrabbed  = false;
        break;
    case(4):
        CentralPointGrabbed = false;
        FirstPointGrabbed   = false;
        SecondPointGrabbed  = true;
        break;
    case(5):
        CentralPointGrabbed = false;
        FirstPointGrabbed   = false;
        SecondPointGrabbed  = false;
        break;
    }
}

void LineSelection::Recalculate()
{
    x1 = Item->FrameObj.x1;
    y1 = Item->FrameObj.y1;

    x2 = Item->FrameObj.x2;
    y2 = Item->FrameObj.y2;

    CentralX = x1 + (x2 - x1)/2;
    CentralY = y1 + (y2 - y1)/2;
}

LineSelection::~LineSelection()
{

}
