#pragma once
#include "Model/SelectionClasses/selection.h"

class LineSelection : public Selection
{
public:

    LineSelection(GraphItem* Item);

    virtual bool TryGrab(int x, int y) override;

    virtual void Draw(PainterClass PeterThePainter) override;

    virtual void MoveTo(int x, int y, bool Local = false) override;

    virtual void ChangeGrabbedPoint(unsigned char NewPoint) override;

    virtual void Recalculate() override;

    virtual ~LineSelection() override;

    // First point for which you can change the size
    int x1;
    int y1;

    // Second point for which you can change the size
    int x2;
    int y2;

private:

    bool FirstPointGrabbed;
    bool SecondPointGrabbed;
};
