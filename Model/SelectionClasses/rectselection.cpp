#include "rectselection.h"
#include "Model/GraphItemAndChilds/graphitem.h"
#include "Model/FacadeStuff/painterclass.h"

RectSelection::RectSelection(GraphItem* Item) : LineSelection(Item)
{
    FirstPointGrabbed = false;
    SecondPointGrabbed = false;
    ThirdPointGrabbed = false;
    FourthPointGrabbed = false;
    CentralPointGrabbed = false;

    x1 = Item->FrameObj.x1;
    y1 = Item->FrameObj.y1;

    x2 = Item->FrameObj.x2;
    y2 = y1;

    x3 = x1;
    y3 = Item->FrameObj.y2;

    x4 = Item->FrameObj.x2;
    y4 = Item->FrameObj.y2;

    CentralX = x1 + (x4 - x1)/2;
    CentralY = y1 + (y4 - y1)/2;
}

bool RectSelection::TryGrab(int x, int y)
{
    int delta = 12;

    // Check if user is trying to grab first point
    if ( (x <= (x1 + delta)) && (x >= (x1 - delta)) )
    {
        // x axis is correct
        if ( (y <= (y1 + delta)) && (y >= (y1 - delta)) )
        {
            // y axis is correct too
            FirstPointGrabbed = true;
            SecondPointGrabbed = false;
            ThirdPointGrabbed = false;
            FourthPointGrabbed = false;
            CentralPointGrabbed = false;
            return true;
        }
    }
    // Check if user is trying to grab second point
    if ( (x <= (x2 + delta)) && (x >= (x2 - delta)) )
    {
        // x axis is correct
        if ( (y <= (y2 + delta)) && (y >= (y2 - delta)) )
        {
            // y axis is correct too
            FirstPointGrabbed = false;
            SecondPointGrabbed = true;
            ThirdPointGrabbed = false;
            FourthPointGrabbed = false;
            CentralPointGrabbed = false;
            return true;
        }
    }
    // Check if user is trying to grab third point
    if ( (x <= (x3 + delta)) && (x >= (x3 - delta)) )
    {
        // x axis is correct
        if ( (y <= (y3 + delta)) && (y >= (y3 - delta)) )
        {
            // y axis is correct too
            FirstPointGrabbed = false;
            SecondPointGrabbed = false;
            ThirdPointGrabbed = true;
            FourthPointGrabbed = false;
            CentralPointGrabbed = false;
            return true;
        }
    }
    // Check if user is trying to grab fourth point
    if ( (x <= (x4 + delta)) && (x >= (x4 - delta)) )
    {
        // x axis is correct
        if ( (y <= (y4 + delta)) && (y >= (y4 - delta)) )
        {
            // y axis is correct too
            FirstPointGrabbed = false;
            SecondPointGrabbed = false;
            ThirdPointGrabbed = false;
            FourthPointGrabbed = true;
            CentralPointGrabbed = false;
            return true;
        }
    }
    // Check if user is trying to grab central point
    if ( (x <= (CentralX + delta)) && (x >= (CentralX - delta)) )
    {
        // x axis is correct
        if ( (y <= (CentralY + delta)) && (y >= (CentralY - delta)) )
        {
            // y axis is correct too
            FirstPointGrabbed = false;
            SecondPointGrabbed = false;
            ThirdPointGrabbed = false;
            FourthPointGrabbed = false;
            CentralPointGrabbed = true;
            return true;
        }
    }
    CentralPointGrabbed = false;
    FirstPointGrabbed = false;
    SecondPointGrabbed = false;
    ThirdPointGrabbed = false;
    FourthPointGrabbed = false;
    return false;
}

void RectSelection::Draw(PainterClass PeterThePainter)
{
    int delta = 20;

    PeterThePainter.DrawSelectionPoint(x1,y1,delta);
    PeterThePainter.DrawSelectionPoint(x2,y2,delta);
    PeterThePainter.DrawSelectionPoint(x3,y3,delta);
    PeterThePainter.DrawSelectionPoint(x4,y4,delta);
    PeterThePainter.DrawSelectionPoint(CentralX,CentralY,delta);
}

void RectSelection::MoveTo(int x, int y, bool Local)
{
    if (Local == true)
    {
        // This element is in a group and this command came from group master
        // x and y is a local shift from GrabbedPoint

        Item->FrameObj.x1 += x;
        Item->FrameObj.y1 += y;
        Item->FrameObj.x2 += x;
        Item->FrameObj.y2 += y;

        x1 += x;
        y1 += y;
        x2 += x;
        y2 += y;
        x3 += x;
        y3 += y;
        x4 += x;
        y4 += y;
    }
    else if (FirstPointGrabbed)
    {
        Item->FrameObj.x1 = x;
        Item->FrameObj.y1 = y;

        x1 = x;
        y1 = y;

        y2 += y - y2;
        x3 += x - x3;
    }
    else if (SecondPointGrabbed)
    {
        Item->FrameObj.x2 = x;
        Item->FrameObj.y1 = y;

        x2 = x;
        y2 = y;

        y1 += y - y1;
        x4 += x - x4;
    }
    else if (ThirdPointGrabbed)
    {
        Item->FrameObj.x1 = x;
        Item->FrameObj.y2 = y;

        x3 = x;
        y3 = y;

        x1 += x - x1;
        y4 += y - y4;
    }
    else if (FourthPointGrabbed)
    {
        Item->FrameObj.x2 = x;
        Item->FrameObj.y2 = y;

        x4 = x;
        y4 = y;

        x2 += x - x2;
        y3 += y - y3;
    }
    else if (CentralPointGrabbed)
    {
        Item->FrameObj.x1 += x - CentralX;
        Item->FrameObj.y1 += y - CentralY;
        Item->FrameObj.x2 += x - CentralX;
        Item->FrameObj.y2 += y - CentralY;

        x1 += x - CentralX;
        y1 += y - CentralY;
        x2 += x - CentralX;
        y2 += y - CentralY;
        x3 += x - CentralX;
        y3 += y - CentralY;
        x4 += x - CentralX;
        y4 += y - CentralY;
    }

    CentralX = x1 + (x4 - x1)/2;
    CentralY = y1 + (y4 - y1)/2;
}

void RectSelection::ChangeGrabbedPoint(unsigned char NewPoint)
{
    switch(NewPoint)
    {
    case(0):
        CentralPointGrabbed = true;
        FirstPointGrabbed   = false;
        SecondPointGrabbed  = false;
        ThirdPointGrabbed   = false;
        FourthPointGrabbed  = false;
        break;
    case(1):
        CentralPointGrabbed = false;
        FirstPointGrabbed   = true;
        SecondPointGrabbed  = false;
        ThirdPointGrabbed   = false;
        FourthPointGrabbed  = false;
        break;
    case(2):
        CentralPointGrabbed = false;
        FirstPointGrabbed   = false;
        SecondPointGrabbed  = true;
        ThirdPointGrabbed   = false;
        FourthPointGrabbed  = false;
        break;
    case(3):
        CentralPointGrabbed = false;
        FirstPointGrabbed   = false;
        SecondPointGrabbed  = false;
        ThirdPointGrabbed   = true;
        FourthPointGrabbed  = false;
        break;
    case(4):
        CentralPointGrabbed = false;
        FirstPointGrabbed   = false;
        SecondPointGrabbed  = false;
        ThirdPointGrabbed   = false;
        FourthPointGrabbed  = true;
        break;
    case(5):
        CentralPointGrabbed = false;
        FirstPointGrabbed   = false;
        SecondPointGrabbed  = false;
        ThirdPointGrabbed   = false;
        FourthPointGrabbed  = false;
        break;
    }
}

void RectSelection::Recalculate()
{
    x1 = Item->FrameObj.x1;
    y1 = Item->FrameObj.y1;

    x2 = Item->FrameObj.x2;
    y2 = y1;

    x3 = x1;
    y3 = Item->FrameObj.y2;

    x4 = Item->FrameObj.x2;
    y4 = Item->FrameObj.y2;

    CentralX = x1 + (x4 - x1)/2;
    CentralY = y1 + (y4 - y1)/2;
}

RectSelection::~RectSelection()
{

}
