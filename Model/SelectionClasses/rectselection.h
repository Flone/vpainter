#pragma once
#include "Model/SelectionClasses/lineselection.h"

class RectSelection : public LineSelection
{
public:

    RectSelection(GraphItem* Item);

    virtual bool TryGrab(int x, int y) override;

    virtual void Draw(PainterClass PeterThePainter) override;

    virtual void MoveTo(int x, int y, bool Local = false) override;

    virtual void ChangeGrabbedPoint(unsigned char NewPoint) override;

    virtual void Recalculate() override;

    virtual ~RectSelection() override;

    // Third point for which you can change the size
    int x3;
    int y3;

    // Fourth point for which you can change the size
    int x4;
    int y4;

private:

    bool FirstPointGrabbed;
    bool SecondPointGrabbed;
    bool ThirdPointGrabbed;
    bool FourthPointGrabbed;
};
