#pragma once

class GraphItem;
class PainterClass;

class Selection
{
public:

    Selection(GraphItem* Item);

    virtual bool TryGrab(int x, int y) = 0;

    virtual void MoveTo(int x, int y, bool Local = false) = 0;

    virtual void ChangeGrabbedPoint(unsigned char NewPoint) = 0;

    virtual void Draw(PainterClass PeterThePainter) = 0;

    virtual void Recalculate() = 0;

    virtual ~Selection();

    GraphItem* Item;

    int CentralX;
    int CentralY;

protected:

    bool CentralPointGrabbed;
};
