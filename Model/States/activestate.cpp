#include "activestate.h"

ActiveState::ActiveState(EditorFacade* pFacade, ControllerClass* pController, BaseState* pCurrentState) : BaseState (pFacade,pController)
{
    this->pCurrentState = pCurrentState;
}

void ActiveState::SetCurrentState(BaseState *pNewState)
{
    pCurrentState = pNewState;
}

ActiveState::~ActiveState()
{

}

void ActiveState::MouseMove(int x, int y)
{
    pCurrentState->MouseMove(x,y);
}

void ActiveState::MouseDown(int x, int y, unsigned char ButtonID, bool CtrlPressed)
{
    pCurrentState->MouseDown(x,y,ButtonID, CtrlPressed);
}

void ActiveState::MouseUp(int x, int y, unsigned char ButtonID)
{
    pCurrentState->MouseUp(x,y,ButtonID);
}
