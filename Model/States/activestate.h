#pragma once
#include "Model/States/basestate.h"

class ControllerClass;

class ActiveState : public BaseState
{
public:

    ActiveState(EditorFacade* pFacade, ControllerClass* pController, BaseState* pCurrentState);

    void SetCurrentState(BaseState* pNewState);

    virtual ~ActiveState() override;

    virtual void MouseMove(int x, int y) override;

    virtual void MouseDown(int x, int y, unsigned char ButtonID, bool CtrlPressed = false) override;

    virtual void MouseUp(int x, int y, unsigned char ButtonID) override;

private:

    BaseState* pCurrentState;
};
