#include "basestate.h"
#include "Model/FacadeStuff/editorfacade.h"

BaseState::BaseState(EditorFacade* pFacade, ControllerClass* pController)
{
    this->pFacade = pFacade;
    this->pController = pController;
}

BaseState::~BaseState()
{
}
