#pragma once

class EditorFacade;
class ControllerClass;

class BaseState
{
public:

    BaseState(EditorFacade* pFacade, ControllerClass* pController);

    virtual ~BaseState();

    virtual void MouseMove(int x, int y) = 0;

    virtual void MouseDown(int x, int y, unsigned char ButtonID, bool CtrlPressed = false) = 0;

    virtual void MouseUp(int x, int y, unsigned char ButtonID) = 0;

protected:

    EditorFacade* pFacade;

    ControllerClass* pController;
};
