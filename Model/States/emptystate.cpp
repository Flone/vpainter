#include "emptystate.h"
#include "Model/FacadeStuff/editorfacade.h"
#include "Controller/controllerclass.h"

EmptyState::EmptyState(EditorFacade* pFacade, ControllerClass* pController) : BaseState (pFacade,pController)
{
}

EmptyState::~EmptyState()
{

}

void EmptyState::MouseMove(int x, int y)
{
    // Nothing here
}

void EmptyState::MouseDown(int x, int y, unsigned char ButtonID, bool CtrlPressed)
{
    if ( (ButtonID == 10) && (pFacade->CheckHit(x,y)) )
    {
        // Change active state to SingleSelect
        pController->SetNewActiveState(2);
    }
    else if (ButtonID != 10)
    {  
        pFacade->CreateObject(x,y,ButtonID);

        // Change active state to Obj. Creation
        pController->SetNewActiveState(3);
    }
}

void EmptyState::MouseUp(int x, int y, unsigned char ButtonID)
{
    // Nothing here
}

