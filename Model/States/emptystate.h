#pragma once
#include "Model/States/basestate.h"

class ControllerClass;

class EmptyState : public BaseState
{
public:

    EmptyState(EditorFacade* pFacade, ControllerClass* pController);

    virtual ~EmptyState() override;

    virtual void MouseMove(int x, int y) override;

    virtual void MouseDown(int x, int y, unsigned char ButtonID, bool CtrlPressed = false) override;

    virtual void MouseUp(int x, int y, unsigned char ButtonID) override;
};
