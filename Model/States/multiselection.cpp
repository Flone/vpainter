#include "multiselection.h"
#include "Model/FacadeStuff/editorfacade.h"
#include "Controller/controllerclass.h"

MultiSelection::MultiSelection(EditorFacade* pFacade, ControllerClass* pController) : BaseState (pFacade, pController)
{

}

MultiSelection::~MultiSelection()
{

}

void MultiSelection::MouseMove(int x, int y)
{

}

void MultiSelection::MouseDown(int x, int y, unsigned char ButtonID, bool CtrlPressed)
{
    if ((ButtonID == 3) && (pFacade->CheckHit(x,y)))
    {
        // User want to make a group

        pFacade->CreateObject(x,y,ButtonID);

        // Disable Group button
        pFacade->ShowUnShowGroupButton(false);

        // Set current instrument to 'Cursor'
        pController->ButtonID = 10;

        // Change active state to Single Select
        pController->SetNewActiveState(2);
    }
    else if (!pFacade->CheckHit(x,y))
    {
        //Miss
        pFacade->DeleteAllSelections();

        pFacade->ShowUnShowGroupButton(false);

        // User swiched selection from one object to other

        // Set current instrument to Cursor
        pController->ButtonID = 10;

        // Change active state to Empty state
        pController->SetNewActiveState(0);
    }
}

void MultiSelection::MouseUp(int x, int y, unsigned char ButtonID)
{

}

