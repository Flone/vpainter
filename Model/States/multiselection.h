#pragma once
#include "Model/States/basestate.h"

class ControllerClass;

class MultiSelection : public BaseState
{
public:

    MultiSelection(EditorFacade* pFacade, ControllerClass* pController);

    virtual ~MultiSelection() override;

    virtual void MouseMove(int x, int y) override;

    virtual void MouseDown(int x, int y, unsigned char ButtonID, bool CtrlPressed = false) override;

    virtual void MouseUp(int x, int y, unsigned char ButtonID) override;
};
