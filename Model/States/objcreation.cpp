#include "objcreation.h"
#include "Model/FacadeStuff/editorfacade.h"
#include "Controller/controllerclass.h"

ObjCreation::ObjCreation(EditorFacade* pFacade, ControllerClass* pController) : BaseState (pFacade, pController)
{

}

ObjCreation::~ObjCreation()
{
}

void ObjCreation::MouseMove(int x, int y)
{
    // Nothing here
}

void ObjCreation::MouseDown(int x, int y, unsigned char ButtonID, bool CtrlPressed)
{
    // Nothing here
}

void ObjCreation::MouseUp(int x, int y, unsigned char ButtonID)
{
    pFacade->CreateObject(x,y,ButtonID);

    // Change active state to Single select
    pController->SetNewActiveState(2);
}

