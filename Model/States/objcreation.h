#pragma once
#include "Model/States/basestate.h"

class ControllerClass;

class ObjCreation : public BaseState
{
public:

    ObjCreation(EditorFacade* pFacade, ControllerClass* pController);

    virtual ~ObjCreation() override;

    virtual void MouseMove(int x, int y) override;

    virtual void MouseDown(int x, int y, unsigned char ButtonID, bool CtrlPressed = false) override;

    virtual void MouseUp(int x, int y, unsigned char ButtonID) override;
};
