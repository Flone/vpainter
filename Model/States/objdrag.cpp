#include "objdrag.h"
#include "Model/FacadeStuff/editorfacade.h"
#include "Controller/controllerclass.h"

ObjDrag::ObjDrag(EditorFacade* pFacade, ControllerClass* pController) : BaseState (pFacade, pController)
{
    CommandCreated = false;
}

ObjDrag::~ObjDrag()
{

}

void ObjDrag::MouseMove(int x, int y)
{
    pFacade->MoveTo(x,y);

    pFacade->MouseDownOnSelectionPoint(false);
}

void ObjDrag::MouseDown(int x, int y, unsigned char ButtonID, bool CtrlPressed)
{
    // Nothing here
}

void ObjDrag::MouseUp(int x, int y, unsigned char ButtonID)
{
    CommandCreated = false;

    // Change active state to Single select
    pController->SetNewActiveState(2);
}
