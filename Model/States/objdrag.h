#pragma once
#include "Model/States/basestate.h"

class ControllerClass;

class ObjDrag : public BaseState
{
public:

    ObjDrag(EditorFacade* pFacade, ControllerClass* pController);

    virtual ~ObjDrag() override;

    virtual void MouseMove(int x, int y) override;

    virtual void MouseDown(int x, int y, unsigned char ButtonID, bool CtrlPressed = false) override;

    virtual void MouseUp(int x, int y, unsigned char ButtonID) override;

private:

    // 'True' when we first time moved something, 'false' if active state is no longer this one
    // We need that variable because MoveTo is being called for every micro move by mouse and
    // on every micro move we will create DragCommand so user will need to make Undo() a lot
    // of times but we don't want that
    bool CommandCreated;
};
