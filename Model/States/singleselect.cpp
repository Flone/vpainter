#include "singleselect.h"
#include "Model/FacadeStuff/editorfacade.h"
#include "Controller/controllerclass.h"

SingleSelect::SingleSelect(EditorFacade* pFacade, ControllerClass* pController) : BaseState(pFacade, pController)
{

}

SingleSelect::~SingleSelect()
{

}

void SingleSelect::MouseMove(int x, int y)
{
    // Nothing here
}

void SingleSelect::MouseDown(int x, int y, unsigned char ButtonID, bool CtrlPressed)
{
    if (CtrlPressed == true)
    {
        if (pFacade->CheckHit(x,y))
        {
            // User selected more that one elements
            // Change active state to Multi Select
            pController->SetNewActiveState(4);
        }
    }
    else
    {
        if ((ButtonID == 3) && (pFacade->CheckHit(x,y)))
        {
            pFacade->UnGroup();

            pFacade->DeleteAllSelections();

            // Set current instrument to Cursor
            pController->ButtonID = 10;

            // Change active state to Empty state
            pController->SetNewActiveState(0);
        }
        else if (pFacade->TryGrab(x,y))
        {
            pFacade->MouseDownOnSelectionPoint(true);

            // Change active state to Obj. Drag
            pController->SetNewActiveState(1);
        }
        else
        {
            // Miss
            pFacade->DeleteAllSelections();

            // Set current instrument to Cursor
            pController->ButtonID = 10;

            // Change active state to Empty state
            pController->SetNewActiveState(0);
        }
    }
}

void SingleSelect::MouseUp(int x, int y, unsigned char ButtonID)
{
    // Nothing here
}
