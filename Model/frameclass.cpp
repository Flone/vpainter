#include "frameclass.h"


FrameClass FrameClass::Compose(FrameClass Frame)
{
    FrameClass ComposedFrame;

    // Set x
    ComposedFrame.x1 = x1 < Frame.x1 ? x1 : Frame.x1;
    ComposedFrame.x2 = x2 > Frame.x2 ? x2 : Frame.x2;
    // Set y
    ComposedFrame.y1 = y1 < Frame.y1 ? y1 : Frame.y1;
    ComposedFrame.y2 = y2 > Frame.y2 ? y2 : Frame.y2;

    return ComposedFrame;
}

int FrameClass::LeftX()
{
    return x1 < x2 ? x1 : x2;
}

int FrameClass::RightX()
{
    return x1 > x2 ? x1 : x2;
}

int FrameClass::TopY()
{
    return y1 < y2 ? y1 : y2;
}

int FrameClass::BottomY()
{
    return y1 > y2 ? y1 : y2;
}
