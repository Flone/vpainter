#pragma once

class FrameClass
{
public:

    FrameClass Compose(FrameClass Frame);

    int LeftX();

    int RightX();

    int TopY();

    int BottomY();

    int x1,y1,x2,y2;
};
