#-------------------------------------------------
#
# Project created by QtCreator 2018-10-30T20:00:47
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VPainter
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        viewwindow.cpp \
    Controller/controllerclass.cpp \
    Model/FacadeStuff/editorfacade.cpp \
    Model/FacadeStuff/sceneclass.cpp \
    Model/FacadeStuff/factory.cpp \
    Model/FacadeStuff/storeclass.cpp \
    Model/FacadeStuff/painterclass.cpp \
    Model/GraphItemAndChilds/group.cpp \
    Model/GraphItemAndChilds/atomgraphitem.cpp \
    Model/GraphItemAndChilds/lineclass.cpp \
    Model/GraphItemAndChilds/rectclass.cpp \
    Model/GraphItemAndChilds/ellipseclass.cpp \
    Model/PropsClasses/propbox.cpp \
    Model/GraphItemAndChilds/graphitem.cpp \
    Model/States/basestate.cpp \
    Model/States/activestate.cpp \
    Model/States/emptystate.cpp \
    Model/States/objdrag.cpp \
    Model/States/singleselect.cpp \
    Model/States/objcreation.cpp \
    Model/States/multiselection.cpp \
    Model/frameclass.cpp \
    Model/FacadeStuff/selectionstore.cpp \
    Model/SelectionClasses/selection.cpp \
    Model/SelectionClasses/lineselection.cpp \
    Model/SelectionClasses/rectselection.cpp \
    Model/SelectionClasses/groupselection.cpp \
    Model/Commands/command.cpp \
    Model/Commands/dragcommand.cpp \
    Model/Commands/objcreatecommand.cpp \
    Model/Commands/groupcommand.cpp \
    Model/Commands/ungroupcommand.cpp

HEADERS += \
        viewwindow.h \
    Controller/controllerclass.h \
    Model/FacadeStuff/editorfacade.h \
    Model/FacadeStuff/sceneclass.h \
    Model/FacadeStuff/factory.h \
    Model/FacadeStuff/storeclass.h \
    Model/FacadeStuff/painterclass.h \
    Model/frameclass.h \
    Model/GraphItemAndChilds/graphitem.h \
    Model/GraphItemAndChilds/group.h \
    Model/GraphItemAndChilds/atomgraphitem.h \
    Model/GraphItemAndChilds/lineclass.h \
    Model/GraphItemAndChilds/rectclass.h \
    Model/GraphItemAndChilds/ellipseclass.h \
    Model/PropsClasses/propbox.h \
    Model/PropsClasses/props.h \
    Model/PropsClasses/penprops.h \
    Model/PropsClasses/brushprop.h \
    Model/States/basestate.h \
    Model/States/activestate.h \
    Model/States/emptystate.h \
    Model/States/objdrag.h \
    Model/States/singleselect.h \
    Model/States/objcreation.h \
    Model/States/multiselection.h \
    Model/FacadeStuff/selectionstore.h \
    Model/SelectionClasses/selection.h \
    Model/SelectionClasses/lineselection.h \
    Model/SelectionClasses/rectselection.h \
    Model/SelectionClasses/groupselection.h \
    Model/Commands/command.h \
    Model/Commands/dragcommand.h \
    Model/Commands/objcreatecommand.h \
    Model/Commands/groupcommand.h \
    Model/Commands/ungroupcommand.h

FORMS += \
        viewwindow.ui


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
