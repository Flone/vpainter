#include "viewwindow.h"
#include "ui_viewwindow.h"
#include "Controller/controllerclass.h"

#include <QKeyEvent>

ViewWindow::ViewWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ViewWindow)
{
    ui->setupUi(this);

    pController = new ControllerClass(this);

    CtrlPressed = false;
}

ViewWindow::~ViewWindow()
{
    delete pController;
    delete ui;
}

void ViewWindow::on_ExitAction_triggered()
{
    QApplication::quit();
}

void ViewWindow::on_lineButton_clicked()
{
    pController->LineButtonPressed();
}

void ViewWindow::on_rectButton_clicked()
{
    pController->RectButtonPressed();
}

void ViewWindow::on_ellipseButton_clicked()
{
    pController->EllipseButtonPressed();
}


void ViewWindow::keyPressEvent(QKeyEvent *event)
{
    if(event->modifiers()== Qt::CTRL)
    {
        CtrlPressed = true;
        pController->CtrlChange(CtrlPressed);
    }
    if (event->key() == Qt::Key_Z)
    {
        pController->ZPressed();
    }
}

void ViewWindow::keyReleaseEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Control)
    {
        CtrlPressed = false;
        pController->CtrlChange(CtrlPressed);
    }
}

void ViewWindow::on_groupButton_clicked()
{
    pController->GroupButtonPressed();
}

void ViewWindow::on_horizontalSlider_valueChanged(int value)
{
    setWindowOpacity(value/99.00);
}

void ViewWindow::on_CursorButton_clicked()
{
    pController->ButtonID = 10;
}
