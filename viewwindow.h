#pragma once
#include <QMainWindow>

namespace Ui {
class ViewWindow;
}

class ControllerClass;

class ViewWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ViewWindow(QWidget *parent = nullptr);
    virtual ~ViewWindow() override;

    // Main UI
    Ui::ViewWindow  *ui;

protected:

    virtual void keyPressEvent(QKeyEvent *event) override;

    virtual void keyReleaseEvent(QKeyEvent* event) override;

private slots:
    void on_ExitAction_triggered();

    void on_lineButton_clicked();

    void on_rectButton_clicked();

    void on_ellipseButton_clicked();

    void on_groupButton_clicked();

    void on_horizontalSlider_valueChanged(int value);

    void on_CursorButton_clicked();

private:

    // Controller sends the user input to EditorFacade class
    ControllerClass *pController;

    bool CtrlPressed;
};
